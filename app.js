require('dotenv').config();
const http = require("http");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const express = require("express");
const app = express();
const routes = require("./routes/routes");
const port = process.env.PORT || '3000';
http.createServer(app).listen(port, () => {
	console.log("listening on port: " + port);
})

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useCreateIndex: true,
	useNewUrlParser: true
})
mongoose.set('useFindAndModify', false);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}))

app.use(cors());
app.use('/', express.static(__dirname + '/'));
routes(app);
 
