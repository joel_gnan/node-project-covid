const models = require('../models/broadcast-schemas')
module.exports = {
    createBroadcast: (req, res) => {
        let broadcastDetails = req.body;
        models.createBroadcast(broadcastDetails, (err, result) => {
            if (err) return err;
            else {
                return res.status(200).json({
                    status: "success",
                    data: result
                })
            }
        })
    },
    uploadfile: (req, res) => {
        // var XLSX = require('xlsx')
        // var workbook = XLSX.readFile('data.xlsx');
        // var sheet_name_list = workbook.SheetNames;
        // var xlData = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]]);
        // models.createBroadcast(xlData, (err, result) => {
        //     if (err) return err;
        //     else {
        //         return res.status(200).json({
        //             status: "success",
        //             data: result
        //         })
        //     }
        // })
        // res.send(xlData)
        res.send('file uploaded success')
    },

    getData: (req, res) => {
        json = {
            "hr": [
              {
                "name": "Harska",
                "employee_id": "500067",
                "email": "harsha@gmail.com",
                "mobile": "9000900090"
              }
            ],
            "development": [
              {
                "name": "sai",
                "employee_id": "500068",
                "email": "sai@gmail.com",
                "mobile": "9111911191"
              }
            ],
            "IT support": [
              {
                "name": "manoj",
                "employee_id": "500069",
                "email": "manoj@gmail.com",
                "mobile": "9222922292"
              },
              {
                "name": "vijay",
                "employee_id": "500061",
                "email": "vijay@gmail.com",
                "mobile": "9333933393"
              }
            ]
          }
       res.send(json); 
    },
    

}