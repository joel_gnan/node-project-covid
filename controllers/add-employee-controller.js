const models = require('../models/broadcast-employee-schema.js')

module.exports = {
    addEmployee: (req, res) => {
        let employeeDetails = req.body;
        models.createBroadcastEmployee(employeeDetails, (err, result) => {
            if (err) return err;
            else {
                return res.status(200).json({
                    'message': "Employee Created",
                    status: "success",
                    data: result
                })
            }
        })
    }
}