var moment = require('moment');

dateHelpers = {
    currentTime: () => {
        return new moment().unix();
    },

    convertDateToEpoch: (date, format = "DD-MM-YYYY") => {
        if(!date) {
            return;
        }

        return new moment(date, format).valueOf();
    },

    convertEpochToDate: (epoch, format = "DD-MM-YYYY") => {
        if (!epoch) {
            return;
        }

        return new moment(epoch).format(format);
    },

    currentHumanTime : () => {
        return new moment().format("DD-MM-YYYY h:m:s");
    }
}

module.exports = dateHelpers;
