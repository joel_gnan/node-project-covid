const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let broadCastSchema = new Schema({
    file_name: String,
    status: Number,
    count:Number,
    organization_id: String,
}, {
    timestamps: true
}, {
    versionKey: false
})

let createBroadcastModel = mongoose.model('broadcast', broadCastSchema)
createBroadcast = function (json, callback) {
    let broadcastjson = new createBroadcastModel(json);
    broadcastjson.save()
}

module.exports = {
    createBroadcast
}
