const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let usersDetailsSchema = new Schema({
    userName: String,
    empId: String,
    organization: String,
}, {
    timestamps: true
}, {
    versionKey: false
})

let createUserModel = mongoose.model('user-details', usersDetailsSchema)
createUser = function (json, callback) {
    let botJson = new createUserModel(json);
    botJson.save((err, data) => {
        if (err) {
            return callback(err);
        } else {
            return callback(null, data)
        }
    })
}

module.exports = {
    createUser,
    createUserModel
}
